import React, { Component } from "react";
import PropTypes from "prop-types";
import { withComponents } from "@reactioncommerce/components-context";

class SaveButton extends Component {
  static propTypes = {
    components: PropTypes.shape({
      Button: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
    }).isRequired
  };

  render() {
    const { ShippingAddressCheckoutAction } = this.props.components;
    const fulfillment = {
      data: {
        shippingAddress: null
      }
    };
    return <ShippingAddressCheckoutAction fulfillmentGroup={fulfillment} label="Shipping Address" stepNumber={1} />;
  }
}

export default withComponents(SaveButton);